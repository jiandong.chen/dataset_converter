import rospy
import rosbag
import cv2
import cv_bridge
import numpy as np
import dataset_converter.util as util
import bisect
from pathlib import Path
from dataset_converter.msg import PohangGPS
from sensor_msgs.msg import Imu, PointCloud2, PointField, NavSatFix
from geometry_msgs.msg import PoseStamped

# Other setup
ENABLE_OMNI_ROTATION = (
    True  # set true for better visulization, but omni extrinsic cannot be used directly
)
MIN_INFRARED_TEMPERTURE = 20
MAX_INFRARED_TEMPERTURE = 50

# used by process func
# map "folder_name(file_name)" : ("LINK_NAME", "TOPIC_NAME")
# comment if you dont want to process specific files or folders
NAVIGATION_PROCESS = {
    "ahrs.txt": ("gx5_link", "/gx5/imu/data"),
    "baseline.txt": ("gx5_link", "/gx5/baseline"),
    "gps.txt": ("gps_link", "/gps/gps_nav"),
}
LIDAR_PROCESS = {
    "lidar_front/points": (
        "lidar_front/os_sensor",
        "/lidar_front/os_cloud_node/points",
    ),
    "lidar_port/points": ("lidar_port/os_sensor", "/lidar_port/os_cloud_node/points"),
    "lidar_starboard/points": (
        "lidar_starboard/os_sensor",
        "/lidar_starboard/os_cloud_node/points",
    ),
}
LIDAR_IMU_PROCESS = {
    "lidar_front/imu.txt": (
        "lidar_front/imu",
        "/lidar_front/imu/data",
    ),
    "lidar_port/imu.txt": ("lidar_port/imu", "/lidar_port/imu/data"),
    "lidar_starboard/imu.txt": (
        "lidar_starboard/imu",
        "/lidar_starboard/imu/data",
    ),
}
STEREO_PROCESS = {
    "left_images": ("stereo_left_link", "/stereo_cam/left_img/compressed"),
    "right_images": ("stereo_right_link", "/stereo_cam/right_img/compressed"),
}
INFRARED_PROCESS = {
    "images": ("infrared_link", "/infrared/image"),
    "visible": ("infrared_link", "/infrared_visible/image/compressed"),
}
RADAR_PROCESS = {
    "images": ("radar_link", "/radar/image"),
}
OMNI_PROCESS = {
    "cam_0": ("omni0_link", "/omni_cam/cam_0/compressed"),
    "cam_1": ("omni1_link", "/omni_cam/cam_1/compressed"),
    "cam_2": ("omni2_link", "/omni_cam/cam_2/compressed"),
    "cam_3": ("omni3_link", "/omni_cam/cam_3/compressed"),
    "cam_4": ("omni4_link", "/omni_cam/cam_4/compressed"),
    "cam_5": ("omni5_link", "/omni_cam/cam_5/compressed"),
}

# used for checking file existence
# data folder 01 and 05 do NOT have /omni
# comment if you want to ignore specific folders or files
DATA_FOLDER_GENERAL = {
    "calibration",
    "infrared",
    "lidar",
    "navigation",
    # "omni",
    "radar",
    "stereo",
}
NAVIGATION_FOLDER = {"ahrs.txt", "baseline.txt", "gps.txt"}
STEREO_FOLDER = {"left_images", "right_images", "timestamp.txt"}
LIDAR_FOLDER = {"lidar_front", "lidar_port", "lidar_starboard"}
INFRARED_FOLDER = {"images", "timestamp.txt"}
RADAR_FOLDER = {"images", "timestamp.txt"}
OMNI_FOLDER = {"cam_0", "cam_1", "cam_2", "cam_3", "cam_4", "cam_5", "timestamp.txt"}

# used by lidar
LIDAR_POINT_TYPE = [
    ("x", np.float32),
    ("y", np.float32),
    ("z", np.float32),
    ("intensity", np.float32),
    ("time", np.uint32),
    ("reflectivity", np.uint16),
    ("ambient", np.uint16),
    ("range", np.uint32),
]
LIDAR_FIELD = [
    PointField(name="x", offset=0, datatype=PointField.FLOAT32, count=1),
    PointField(name="y", offset=4, datatype=PointField.FLOAT32, count=1),
    PointField(name="z", offset=8, datatype=PointField.FLOAT32, count=1),
    PointField(name="intensity", offset=12, datatype=PointField.FLOAT32, count=1),
    PointField(name="time", offset=16, datatype=PointField.UINT32, count=1),
    PointField(name="reflectivity", offset=20, datatype=PointField.UINT16, count=1),
    PointField(name="ambient", offset=22, datatype=PointField.UINT16, count=1),
    PointField(name="range", offset=24, datatype=PointField.UINT32, count=1),
]
LIDAR_POINT_SIZE = np.dtype(LIDAR_POINT_TYPE).itemsize


def process_folder(data_folder, rosbag_file_dir):
    if not data_folder.is_dir() or not is_datafolder_valid(data_folder):
        return

    rosbag_file_path = rosbag_file_dir.joinpath(data_folder.name + ".bag")
    process_lidar(data_folder, rosbag_file_path)
    process_navigation(data_folder, rosbag_file_path)
    process_stereo(data_folder, rosbag_file_path)
    process_infrared(data_folder, rosbag_file_path)
    process_radar(data_folder, rosbag_file_path)
    process_omni(data_folder, rosbag_file_path)

    print(f'Folder "{data_folder}" Finished! Saved to bag "{rosbag_file_path}"!')


def is_datafolder_valid(folder: Path):
    print(f"Checking Folder: {folder}")

    res = True

    res &= util.is_folder_contain_folders(folder, DATA_FOLDER_GENERAL)
    if res:
        res &= util.is_folder_contain_folders(folder.joinpath("stereo"), STEREO_FOLDER)
    if res:
        res &= util.is_folder_contain_folders(
            folder.joinpath("navigation"), NAVIGATION_FOLDER
        )
    if res:
        res &= util.is_folder_contain_folders(folder.joinpath("lidar"), LIDAR_FOLDER)
    if res:
        res &= util.is_folder_contain_folders(
            folder.joinpath("infrared"), INFRARED_FOLDER
        )
    if res and folder.joinpath("omni").exists():
        res &= util.is_folder_contain_folders(folder.joinpath("omni"), OMNI_FOLDER)

    if res is False:
        print(f'Folder "{folder}" is NOT valid! Skip!')
    else:
        print(f'Folder "{folder}" PASSED!')

    return res


def process_images(
    data_path: Path,
    rosbag_path: Path,
    topic: str,
    frame_id: str,
    compress_fmt=None,
    rotate_img=None,
):
    if not data_path.exists():
        print(f"Path: {data_path} NOT exist! Skip!")
        return

    # read the images path and sort based on file name
    images = util.get_sorted_file_list(data_path)

    # read all lines and divide into [[timestamp, seq], ... ]
    timestamps = get_timestamp_dict(data_path.parent / "timestamp.txt")

    # store all images into bag
    bridge = cv_bridge.CvBridge()
    with rosbag.Bag(rosbag_path, check_bag_mode(rosbag_path)) as bag:
        for img_path in images:
            im_seq = int(img_path.with_suffix("").name)

            if im_seq not in timestamps:
                print(f"Got {img_path} without a valid timestamp! Skip!")
                continue

            t = rospy.Time.from_sec(float(timestamps[im_seq]))

            img = cv2.imread(img_path.as_posix(), cv2.IMREAD_UNCHANGED)

            if rotate_img is not None:
                img = cv2.rotate(img, rotate_img)

            if compress_fmt is not None:
                msg = bridge.cv2_to_compressed_imgmsg(img, dst_format=compress_fmt)
            else:
                msg = bridge.cv2_to_imgmsg(img)

            msg.header.seq = im_seq
            msg.header.stamp = t
            msg.header.frame_id = frame_id

            bag.write(topic, msg, t)


def process_pointcloud(
    data_path: Path,
    rosbag_path: Path,
    topic: str,
    frame_id: str,
):
    if not data_path.exists():
        print(f"Path: {data_path} NOT exist! Skip!")
        return

    pt_files = util.get_sorted_file_list(data_path)

    with rosbag.Bag(rosbag_path, check_bag_mode(rosbag_path)) as bag:
        for idx, f in enumerate(pt_files):
            t = from_nano_sec(int(f.with_suffix("").name))

            pts = np.fromfile(f, dtype=LIDAR_POINT_TYPE)
            msg = PointCloud2()
            msg.header.seq = idx
            msg.header.stamp = t
            msg.header.frame_id = frame_id
            msg.fields = LIDAR_FIELD
            msg.height = 1
            msg.width = pts.shape[0]
            msg.is_dense = False
            msg.is_bigendian = False
            msg.point_step = LIDAR_POINT_SIZE
            msg.row_step = LIDAR_POINT_SIZE * pts.shape[0]
            msg.data = pts.tobytes()

            bag.write(topic, msg, t)


def process_imu(
    data_path: Path,
    rosbag_path: Path,
    topic: str,
    frame_id: str,
):
    if not data_path.exists():
        print(f"Path: {data_path} NOT exist! Skip!")
        return

    data = txt_preprocess(data_path)

    with rosbag.Bag(rosbag_path, check_bag_mode(rosbag_path)) as bag:
        for idx, l in enumerate(data):
            l = list(map(float, l))
            t = rospy.Time.from_sec(l[0])

            imu = Imu()
            imu.header.seq = idx
            imu.header.stamp = t
            imu.header.frame_id = frame_id
            imu.orientation.x = 0
            imu.orientation.y = 0
            imu.orientation.z = 0
            imu.orientation.w = 1
            imu.angular_velocity.x = l[1]
            imu.angular_velocity.y = l[2]
            imu.angular_velocity.z = l[3]
            imu.linear_acceleration.x = l[4]
            imu.linear_acceleration.y = l[5]
            imu.linear_acceleration.z = l[6]

            bag.write(topic, imu, t)


def process_navigation(data_folder: Path, rosbag_path: Path):
    data_path = data_folder.joinpath("navigation")

    # GPS Part
    data = txt_preprocess(data_path.joinpath("gps.txt"))
    with rosbag.Bag(rosbag_path, check_bag_mode(rosbag_path)) as bag:
        for idx, l in enumerate(data):
            l.remove("N")
            l.remove("E")
            l = list(map(float, l))
            t = rospy.Time.from_sec(l[0])

            # NavSatFix Part
            gps = NavSatFix()
            gps.header.seq = idx
            gps.header.stamp = t
            gps.header.frame_id = NAVIGATION_PROCESS["gps.txt"][0]
            gps.latitude = l[2]
            gps.longitude = l[3]
            gps.altitude = l[-1]

            bag.write(NAVIGATION_PROCESS["gps.txt"][1], gps, t)

            # PohangGPS Part
            gps = PohangGPS()
            gps.header.seq = idx
            gps.header.stamp = t
            gps.header.frame_id = NAVIGATION_PROCESS["gps.txt"][0]
            gps.gps_time = l[1]
            gps.latitude = l[2]
            gps.longitude = l[3]
            gps.heading = l[4]
            gps.gps_quality_indicator = int(l[5])
            gps.num_satellites = int(l[6])
            gps.horizontal_dilution_of_precision = l[7]
            gps.geoid = l[8]

            bag.write(NAVIGATION_PROCESS["gps.txt"][1] + "_raw", gps, t)

    # AHRS Part
    data = txt_preprocess(data_path.joinpath("ahrs.txt"))

    with rosbag.Bag(rosbag_path, check_bag_mode(rosbag_path)) as bag:
        for idx, l in enumerate(data):
            l = list(map(float, l))
            t = rospy.Time.from_sec(l[0])

            imu = Imu()
            imu.header.seq = idx
            imu.header.stamp = t
            imu.header.frame_id = NAVIGATION_PROCESS["ahrs.txt"][0]
            imu.orientation.x = l[1]
            imu.orientation.y = l[2]
            imu.orientation.z = l[3]
            imu.orientation.w = l[4]
            imu.angular_velocity.x = l[5]
            imu.angular_velocity.y = l[6]
            imu.angular_velocity.z = l[7]
            imu.linear_acceleration.x = l[8]
            imu.linear_acceleration.y = l[9]
            imu.linear_acceleration.z = l[10]

            bag.write(NAVIGATION_PROCESS["ahrs.txt"][1], imu, t)

    # manully calibrate the time
    t0 = float(data[0][0])
    t_diff = 0.01
    with rosbag.Bag(rosbag_path, check_bag_mode(rosbag_path)) as bag:
        for idx, l in enumerate(data):
            l = list(map(float, l))
            t = rospy.Time.from_sec(t0 + t_diff * idx)  # time calibration

            imu = Imu()
            imu.header.seq = idx
            imu.header.stamp = t
            imu.header.frame_id = NAVIGATION_PROCESS["ahrs.txt"][0]
            imu.orientation.x = l[1]
            imu.orientation.y = l[2]
            imu.orientation.z = l[3]
            imu.orientation.w = l[4]
            imu.angular_velocity.x = l[5]
            imu.angular_velocity.y = l[6]
            imu.angular_velocity.z = l[7]
            imu.linear_acceleration.x = l[8]
            imu.linear_acceleration.y = l[9]
            imu.linear_acceleration.z = l[10]

            bag.write("/gx5/imu_calib/data", imu, t)

    # Baseline Part
    # frame_id unknown
    data = txt_preprocess(data_path.joinpath("baseline.txt"))

    # perform time correction
    # find the first aligned data point
    # all gps time
    gps_time_reference = txt_preprocess(data_path.joinpath("gps.txt"))
    gps_time_reference = [float(gps[0]) for gps in gps_time_reference]
    first_baseline = 0
    first_gps_reference = 0

    while first_baseline < len(data) and first_gps_reference < len(gps_time_reference):
        gps_ref_t = gps_time_reference[first_gps_reference]

        baseline_t = list(map(float, data[first_baseline]))
        baseline_t = baseline_t[0]

        if np.abs(gps_ref_t - baseline_t) <= 0.01:
            # print(
            #     f"Found baseline idx: {first_baseline}, gps idx: {first_gps_reference}"
            # )
            # print(f"Found baseline time: {baseline_t}, gps idx: {gps_ref_t}")
            break

        if gps_ref_t > baseline_t:
            first_baseline += 1
        else:
            first_gps_reference += 1

    gps_reference_idx = first_gps_reference
    with rosbag.Bag(rosbag_path, check_bag_mode(rosbag_path)) as bag:
        for idx, l in enumerate(data[first_baseline:]):
            l = list(map(float, l))

            baseline_t = l[0]

            # find the first gpt_time >= baseline_t
            gps_reference_idx = bisect.bisect_left(
                gps_time_reference, baseline_t, lo=gps_reference_idx
            )
            if gps_reference_idx >= len(gps_time_reference):
                break
            gps_ref_t = gps_time_reference[gps_reference_idx]

            # if error is small, we use original time
            if np.abs(gps_ref_t - baseline_t) <= 0.01:
                t = rospy.Time.from_sec(l[0])
            else:
                # print(
                #     f"Corrected Time {gps_ref_t}(idx: {gps_reference_idx}) baseline idx {idx}"
                # )
                t = rospy.Time.from_sec(gps_ref_t)

            pose = PoseStamped()
            pose.header.seq = idx
            pose.header.stamp = t
            pose.header.frame_id = NAVIGATION_PROCESS["baseline.txt"][0]
            pose.pose.orientation.x = l[1]
            pose.pose.orientation.y = l[2]
            pose.pose.orientation.z = l[3]
            pose.pose.orientation.w = l[4]
            pose.pose.position.x = l[5]
            pose.pose.position.y = l[6]
            pose.pose.position.z = l[7]

            bag.write(NAVIGATION_PROCESS["baseline.txt"][1], pose, t)


def process_stereo(data_folder: Path, rosbag_path: Path):
    data_path = data_folder.joinpath("stereo")
    for k, v in STEREO_PROCESS.items():
        process_images(data_path / k, rosbag_path, v[1], v[0], compress_fmt="png")


def process_lidar(data_folder: Path, rosbag_path: Path):
    data_path = data_folder.joinpath("lidar")
    for k, v in LIDAR_PROCESS.items():
        process_pointcloud(data_path / k, rosbag_path, v[1], v[0])

    for k, v in LIDAR_IMU_PROCESS.items():
        process_imu(data_path / k, rosbag_path, v[1], v[0])


def process_infrared(data_folder: Path, rosbag_path: Path):
    data_path = data_folder.joinpath("infrared")

    # save image file from data
    # generate_visible_infrared(data_path.joinpath("images"))

    for k, v in INFRARED_PROCESS.items():
        process_images(
            data_path / k,
            rosbag_path,
            v[1],
            v[0],
            compress_fmt="png" if k == "visible" else None,
        )


def process_radar(data_folder: Path, rosbag_path: Path):
    data_path = data_folder.joinpath("radar")
    for k, v in RADAR_PROCESS.items():
        process_images(
            data_path / k,
            rosbag_path,
            v[1],
            v[0],
            compress_fmt="png" if k == "visible" else None,
        )


def process_omni(data_folder: Path, rosbag_path: Path):
    data_path = data_folder.joinpath("omni")

    if not data_path.exists():
        return

    for k, v in OMNI_PROCESS.items():
        process_images(
            data_path / k,
            rosbag_path,
            v[1],
            v[0],
            compress_fmt="jpg",
            rotate_img=cv2.ROTATE_90_CLOCKWISE if ENABLE_OMNI_ROTATION else None,
        )


def txt_preprocess(path: Path):
    if not path.exists():
        print(f"Path: {path} NOT exist! Skip!")
        return []

    with open(path, "r") as f:
        data = f.readlines()
        data = [l.strip().split("\t") for l in data]

    return data


def get_timestamp_dict(path: Path):
    if not path.exists():
        print(f"Path: {path} NOT exist! Skip!")
        return {}

    data = {}
    with open(path, "r") as f:
        data = f.readlines()
        data = {
            int(line.strip().split("\t")[1]): line.strip().split("\t")[0]
            for line in data
        }

    return data


def check_bag_mode(rosbag_path: Path):
    if rosbag_path.exists():
        bagmode = "a"
    else:
        bagmode = "w"

    return bagmode


def generate_visible_infrared(image_path: Path):
    to_temperture = np.vectorize(infrared_temperture2pixel)
    target_path = image_path.parent.joinpath("visible")

    print(f"Save Visible Infrared Into {target_path}")
    if target_path.exists():
        if next(target_path.iterdir(), None) is not None:
            print(f"Target Dir is NOT empty, Visible Image Generate Skip!")
    else:
        target_path.mkdir()

    for img_file in image_path.iterdir():
        img = cv2.imread(img_file.as_posix(), cv2.IMREAD_UNCHANGED)
        img = to_temperture(img)
        img = img.astype(np.uint8)

        cv2.imwrite(target_path.joinpath(img_file.name).as_posix(), img)


def infrared_temperture2pixel(data):
    # computer temperture
    temp = data * 0.04 - 273.15
    if temp <= MIN_INFRARED_TEMPERTURE:
        return 0
    elif temp >= MAX_INFRARED_TEMPERTURE:
        return 255  # 2^8 - 1
    else:
        return int(
            (temp - MIN_INFRARED_TEMPERTURE)
            / (MAX_INFRARED_TEMPERTURE - MIN_INFRARED_TEMPERTURE)
            * 255
        )


def from_nano_sec(nano_sec):
    t = rospy.Time()
    t.nsecs = nano_sec
    t = rospy.Time().from_sec(t.to_sec())
    return t
