from pathlib import Path


def is_folder_contain_folders(folder: Path, required_folder: set):
    if not folder.exists():
        print(f"Folder: {folder} NOT exist!")

    temp = {x.name for x in folder.iterdir()}

    if not temp.issuperset(required_folder):
        print(f"Folder: {folder} requires {required_folder - temp}")
        return False

    return True


def get_sorted_file_list(target_path: Path, sort_key=None):
    # default sorted by seq id in file name

    if sort_key is None:
        sort_key = lambda path: path.with_suffix("").parts[-1]
    res = sorted(
        [img for img in target_path.iterdir()],
        key=sort_key,
    )

    return res
