#!/usr/bin/env python3

import yaml
import dataset_converter.pohang as pohang
from pathlib import Path
from concurrent.futures import ProcessPoolExecutor

# See More Config in dataset_converter.pohang
CONFIG_FILE_PATH = Path(__file__).resolve().parents[1] / "config/config.yaml"


if __name__ == "__main__":
    with CONFIG_FILE_PATH.open() as f:
        # get data folder and target rosbag from config file
        config_file = yaml.safe_load(f)["pohang"]
        data_path = Path(config_file["data_folder"])
        rosbag_file_dir = Path(config_file["rosbag_file"])

    with ProcessPoolExecutor() as exec:
        # process all dataset folders
        futures = [
            exec.submit(pohang.process_folder, data_folder, rosbag_file_dir)
            for data_folder in data_path.iterdir()
        ]
