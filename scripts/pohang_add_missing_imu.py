#!/usr/bin/env python3

# add missing lidar imu data
import dataset_converter.pohang as pohang
from pathlib import Path
from concurrent.futures import ProcessPoolExecutor

# pohang folder -> rosbag path
DATA_MAP = {
    "/workspace/data/pohang00_pier/lidar": "/workspace/data/pohang00_pier.bag",
    "/workspace/data/pohang00_canal/lidar": "/workspace/data/pohang00_canal.bag",
}


def add_missing_imu(pohang_data_folder: Path, bag_path: Path):
    for k, v in pohang.LIDAR_IMU_PROCESS.items():
        pohang.process_imu(pohang_data_folder.joinpath(k), bag_path, v[1], v[0])

    print(f"{pohang_data_folder} Done")


if __name__ == "__main__":

    for data_folder, bag_path in DATA_MAP.items():
        add_missing_imu(Path(data_folder), Path(bag_path))

